import ast

opath = '/Users/mfischer/PycharmProjects/tutorial/pygks2016/data/titanic_data.csv'


trueish = ('Survived', True, 'Yes', 1)
falseish = ('Unaccounted', False, 'No', 0)
bool_eval = dict.fromkeys(trueish, True)
bool_eval.update((val, False) for val in falseish)


def super_eval(obj):
    try:
        obj = ast.literal_eval(obj)
    except (ValueError, SyntaxError):
        return obj
    else:
        return bool_eval.get(obj, obj)


def data_parser(line_iter):
    line_buffer = None
    for line in line_iter:
        line = line.rstrip()
        if not line:
            continue
        if not line_buffer:
            line_buffer = line
        elif line.startswith(' '):
            line_buffer += ',' + line.strip()
        else:
            yield [super_eval(elem) for elem in line_buffer.split(':')]
            line_buffer = line

with open(opath) as titanic_data:
    for elements in data_parser(titanic_data):
        print(elements)