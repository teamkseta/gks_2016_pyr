import csv
import ast
import random
import os

tpath = os.path.join(os.path.dirname(__file__), 'titanic.csv')
opath = os.path.join(os.path.dirname(__file__), 'titanic_data.csv')
lpath = os.path.join(os.path.dirname(__file__), 'titanic_passengers.csv')


trueish = ('Survived', True, 'Yes', 1)
falseish = ('Unaccounted', False, 'No', 0)


def string_garbler(obj, garble_chance=0.1, garble_veto=0.0, garble_chars='\0\4\t\a\b'):
    if not isinstance(obj, str) or random.random() < garble_veto:
        return obj
    else:
        chars, idx = list(obj), 0
        while idx < len(chars):
            if random.random() < garble_chance:
                new_char = random.choice(garble_chars)
                print(repr(new_char))
                chars.insert(idx, new_char)
            idx += 1
        chars = ''.join(chars).replace('"', "'")
        return '"' + chars + '"'


def leval(obj):
    try:
        return ast.literal_eval(obj)
    except (ValueError, SyntaxError):
        return obj

with open(tpath, 'r') as inp, open(opath, 'w') as outp:
    reader = csv.reader(inp)
    header = next(reader)
    header.insert(0, 'id')
    outp.write('"' + '":"'.join(header) + '"\n')
    for idx, line in enumerate(csv.reader(inp)):
        if idx % 100:
            print('A', idx, end='\r')
        elements = [leval(element.replace('NA', 'None')) for element in line]
        elements.insert(0, hash(str(idx)) & 0xffffff)
        elements[3] = random.choice(trueish) if elements[3] else random.choice(falseish)
        outp.write(':'.join(repr(element).replace(',', '\n') for element in elements) + '\n')
        if random.random() > 0.9:
            outp.write('\n')

with open(tpath, 'r') as inp, open(lpath, 'w') as outp:
    reader = csv.reader(inp)
    header = next(reader)
    header.insert(0, 'id')
    outp.write('"' + '":"'.join(header) + '"\n')
    for idx, line in enumerate(csv.reader(inp)):
        if idx % 10:
            print('B', idx, end='\r')
        elements = [leval(element.replace('NA', '"NA"')) for element in line]
        elements = [string_garbler(elem) for elem in elements]
        elements.insert(0, hash(str(idx)) & 0xffffff)
        elements[3] = random.choice(trueish) if elements[3] else random.choice(falseish)
        outp.write(':'.join(str(elem) for elem in elements) + '\n')
        if random.random() > 0.9:
            outp.write('\n')
