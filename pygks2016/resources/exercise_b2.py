import datetime
import time
import socket
import random
import weakref
import unittest

from gks2016rpylib import exercise

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))
    localhost_ip = s.getsockname()[0]
except Exception:
    localhost_ip = '.'.join(str(random.randint(1, 255)) for _ in range(4))


def _fake_data(obj):
    reply = {
        'key': id(obj),
        'stmp': time.time(),
        'when': str(datetime.date.today()),
        'connector': 'https://%s:8080/mserve/%2X/%6X' % (localhost_ip, hash(id(obj)) & 0xff, hash(id(obj)) & 0xffffff),
        'size': ((hash(id(obj)) + 5000) % 700) + 700,
        'ress': ((hash(id(obj)) + 5000) % 700) + 672,
        'roundtrip': random.gauss(0.5, 0.1)
    }
    return reply


def query_metadata(obj):
    time.sleep(0.5)
    return _fake_data(obj)


class Data:
    """Some data. Fell off a truck or something"""
    def __init__(self):
        self._data = (('%08X' % (id(self) & 0xffffffff)) + '%056X' % random.getrandbits(4 * 56)).encode('ASCII')

    def __str__(self):
        return str(self._data)

    def __repr__(self):
        return '%s(%s)' % (self.__class__.__name__, self._data)


def get_metadata(obj):
    if obj not in get_metadata.cache:
        get_metadata.cache[obj] = query_metadata(obj)['size']
    return get_metadata.cache[obj]
get_metadata.cache = weakref.WeakKeyDictionary()


class CacheTester(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(
            self.result_candidate,
            type(get_metadata),
            'A *function* must be passed as result (got %s)' % type(self.result_candidate)
        )

    def test_result(self):
        input_dummy = Data()
        self.assertEqual(self.result_candidate(input_dummy), _fake_data(input_dummy)['size'])

    def test_delay(self):
        input_dummy = Data()
        # sample
        before = time.time()
        get_metadata(input_dummy)
        sample_duration = time.time() - before
        # warmup
        before = time.time()
        self.result_candidate(input_dummy)
        candidate_duration = time.time() - before
        self.assertGreaterEqual(candidate_duration, 0.5, 'Are you cheating?')
        # cache
        before = time.time()
        self.result_candidate(input_dummy)
        candidate_duration = time.time() - before
        get_metadata(input_dummy)
        self.assertLess(candidate_duration, 0.4, 'Expected speedup from caching (Sample takes %fs)' % sample_duration)


b2 = exercise.Exercise(
    'Nested Generator Functions',
    sample_solution=get_metadata,
    data={'Data': Data, 'query_metadata': query_metadata},
    test_cases=(CacheTester,),
    on_result=None,
    description='Create a cache for `query_metadata` based on `weakref`.',
    hints=(
        'Use `weakref.WeakKeyDict` to cache data.',
        'The `Data` class supplied by the exercise can be weakref\'d.',
        'Either test whether an object is `in` the cache, or catch the exception.',
        'Your cache must be outside your function.',
    ),
    hint_offset=2,
)
