import unittest
import random
import collections.abc

from gks2016rpylib import exercise


class AbortTransaction(Exception):
    """Abort a Transaction"""


class Transaction:
    def __init__(self, mapping):
        self.mapping = mapping
        self._changes = None

    def __enter__(self):
        assert self._changes is None, 'Context is not reentrant'
        self._changes = {}
        return self._changes

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            if exc_type == AbortTransaction:
                return True
            return False  # do not supress regular errors
        self.mapping.update(self._changes)
        self._changes = None


class TransactionView(collections.abc.MutableMapping):
    """
    View on a Transaction

    Mapping that overlays another mapping with changes. The overlay
    transparently implements insertion and deletion of keys. Values
    are transparently provided, reflecting the insertions and deletions
    on top of the overlay'd mapping.

    Provides a single, additional method, `apply`. This will commit
    all changes to the overlay'd mapping
    """
    def __init__(self, parent):
        self._parent = parent
        self._data = {}  # new content to apply
        self._whiteout = set()  # keys to delete

    def __getitem__(self, key):
        """self[key]"""
        if key in self._whiteout:
            raise KeyError(key)
        try:
            return self._data[key]
        except KeyError:
            return self._parent[key]

    def __setitem__(self, key, value):
        """self[key] = value"""
        self._data[key] = value
        try:
            self._whiteout.remove(key)
        except KeyError:
            pass

    def __delitem__(self, key):
        """del self[key]"""
        self._whiteout.add(key)

    def __iter__(self):
        """iter(self)"""
        # yield overwritten data first
        # and do not repeat keys
        for key in self._data:
            if key not in self._whiteout:
                yield key
        for key in self._parent:
            if key not in self._whiteout and key not in self._data:
                yield key

    def __len__(self):
        """len(self), bool(self)"""
        return len(set(self))

    def apply(self):
        """
        Apply the contained changes

        Applies all recorded changes to its parent, and flushes the internal
        buffer. The parent and buffer are modified inplace.
        """
        for key in self._whiteout:
            self._data.pop(key, None)
            self._parent.pop(key, None)
        self._parent.update(self)
        self.discard()

    def discard(self):
        self._data.clear()

    def __str__(self):
        """str(self)"""
        whiteouts = ', '.join('%r: <~>' % key for key in self._whiteout)
        key_values = repr({key: self[key] for key in self._data if key not in self._whiteout})
        if whiteouts and key_values:
            content = '{%s, %s}' % (whiteouts, key_values[1:-1])
        elif whiteouts:
            content = '{%s}' % whiteouts
        elif key_values:
            content = '{%s}' % key_values[1:-1]
        else:
            content = '{}'
        return '%s >> %s' % (content, self._parent)

    def __repr__(self):
        """repr(self)"""
        return '%s(%s)' % (self.__class__.__name__, self)

    def keys(self):
        return collections.abc.KeysView(self)

    def items(self):
        return collections.abc.ItemsView(self)

    def values(self):
        return collections.abc.ValuesView(self)


class TransparentTransaction(TransactionView):
    """
    Transaction providing a transparent view of the current status
    """
    def __init__(self, parent):
        super().__init__(parent=parent)
        self._context_active = False

    def __enter__(self):
        # guard against re-entrance (i.e. nesting the same context manager)
        assert self._context_active is False, 'Context is not reentrant'
        self._context_active = True
        # transaction view is provided by this instance as well
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._context_active = False
        if exc_type is not None:
            if exc_type == AbortTransaction:
                self.discard()
                return True
            return False  # do not supress regular errors
        self.apply()


class TypeTester(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, type(Transaction), 'A class is required')

    def test_methods(self):
        self.assertTrue(hasattr(self.result_candidate, '__init__'), 'Initializer method (__init__)')
        self.assertTrue(hasattr(self.result_candidate, '__enter__'), 'Context enter method (__enter__)')
        self.assertTrue(hasattr(self.result_candidate, '__exit__'), 'Context exit method (__exit__)')


class InterfaceTester(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_init(self):
        self.assertIsNotNone(self.result_candidate({}))

    def test_context(self):
        with self.result_candidate({}) as changes:
            self.assertIsInstance(changes, collections.abc.MutableMapping)


class InterfaceTransaction(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_context(self):
        initial = {}
        rnum = random.random()
        with self.result_candidate(initial) as changes:
            changes['a'] = 1
            changes[None] = rnum
        self.assertEqual(initial, {'a': 1, None: rnum}, 'Mapping update by transaction')


class InterfaceAbort(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_context(self):
        initial = {'b': 2}
        rnum = random.random()
        with self.result_candidate(initial) as changes:
            changes['a'] = 1
            changes[None] = rnum
            raise AbortTransaction
        self.assertEqual(initial, {'b': 2}, 'Exception aborts transaction')


c1 = exercise.Exercise(
    'Iterable Ring Buffer',
    sample_solution=Transaction,
    data={'AbortTransaction': AbortTransaction},
    test_cases=(TypeTester, InterfaceTester, InterfaceTransaction, InterfaceAbort),
    on_result=None,
    description='Create a Context Manager performing a transaction on a mapping.',
    hints=(
        'Write a class implementing holding the mapping and providing the context manager protocol.',
        'You must provide `__init__`, `__enter__` and `__exit__`.',
        '`__enter__` must return a new dictionary to hold changes.',
        'The changes dictionary must be stored on the instance.',
        'Update the initial mapping with the changes in `__exit__`.',
    ),
    hint_offset=2,
)

