import unittest
import itertools

from gks2016rpylib import exercise
from .exercise_b2 import BTestGeneratorFunctionType


def fibonacci_gen():
    next_fib, prev_fib = 1, 0
    while True:
        yield next_fib
        next_fib, prev_fib = next_fib + prev_fib, next_fib


class B3TestContent(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_infinity(self):
        for test_length in (1, 10, 100, 1000, 5000, 10000):
            self.assertEqual(
                len(list(itertools.islice(self.result_candidate(), test_length))),
                test_length,
                'Produces enough elements (>=%d)' % test_length,
            )

    def test_value(self):
        result_iterator = self.result_candidate()
        sample_iterator = b3.sample_solution()
        for step_size in itertools.chain(range(10)):
            # advance
            result = next(itertools.islice(result_iterator, step_size, step_size + 1), None)
            sample = next(itertools.islice(sample_iterator, step_size, step_size + 1), None)
            self.assertEqual(result, sample)


def _b3_on_result(result, valid, passed_tests):
    if not isinstance(result, type(fibonacci_gen)) and not isinstance(result(), fibonacci_gen()):
        print('That\'s no moon... erm, generator function.')
    print('Generator yields:', end=' ')
    result_values = list(itertools.islice(result(), 20))
    if len(result_values) > 19:
        print(*result_values[:19], end='')
        print('...')
    else:
        print(result_values)
    if not valid:
        sample_values = itertools.islice(fibonacci_gen(), 20)
        print(*sample_values[:19], end='')
        print('...')


b3 = exercise.Exercise(
    'Infinite Generator Function',
    sample_solution=fibonacci_gen,
    data={},
    test_cases=(BTestGeneratorFunctionType, B3TestContent,),
    on_result=_b3_on_result,
    description='Create a generator function for the Fibonacci sequence',
    hints=(
        'Generator functions are defined by `yield value` in their body.',
        'All statements are allowed inside the generator function body.',
        'The Fibonacci sequence is defined recursively.',
        'An infinite loop can be created via `while True:`',
        'You need two variable names.',
    ),
    hint_offset=2,
)
