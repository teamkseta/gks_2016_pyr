import unittest

from gks2016rpylib import exercise

_a3_iterables = (range(0, 20, 2), list(range(0, 40, 6)), set(range(20, 40, 8)), [str(elem) for elem in range(0, 40, 4)])


class A3TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_container_type(self):
        self.assertIsInstance(self.result_candidate, set)

    def test_element_type(self):
        types_in_result = {type(elem) for elem in self.result_candidate}
        self.assertIn(int, types_in_result)
        self.assertEqual(len(types_in_result), 1, 'Same type for all elements')

    def test_value(self):
        self.assertEqual(self.result_candidate, a3.sample_solution)


a3 = exercise.Exercise(
    'Nested Set Comprehension',
    sample_solution={element for iterable in _a3_iterables for element in iterable if isinstance(element, int)},
    data={
        'iterables': _a3_iterables
    },
    test_cases=(A3TestCases,),
    description='Find the unique items of all iterables in `iterables`',
    hints=(
        'Set comprehension syntax is `{element for element in iterable}`',
        'An object\'s type can be checked with `isinstance(element, type)`',
        'You must iterate over all iterables, and all elements of each iterable.',
    ),
    hint_offset=1,
)