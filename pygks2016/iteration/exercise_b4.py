import unittest
import ast
import itertools
import os

from gks2016rpylib import exercise

titanic_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'data', 'titanic_data.csv'))


def _gen_func():
    """Type placeholder"""
    yield 1


def clean_lines(line_iter):
    """Remove line comments and empty lines"""
    for line in line_iter:
        line = line.split('#', 1)[0]
        line = line.rstrip()
        if line:
            yield line


def fix_whitespace(line_iter, separator=','):
    """Remove linebreaks inside fields"""
    line_buffer = None
    for line in line_iter:
        if not line_buffer:
            line_buffer = line
        elif line.startswith(' '):
            line_buffer += separator + line.strip()
        else:
            yield line_buffer
            line_buffer = line


def split_fields(line_iter, separator=','):
    """Split lines into individual fields"""
    for line in line_iter:
        yield line.split(separator)

trueish = ('Survived', True, 'Yes', 1)
falseish = ('Unaccounted', False, 'No', 0)


def parse_fields(elements_iter):
    for elements in elements_iter:
        line = [ast.literal_eval(element) for element in elements]
        line[3] = line[3] in trueish
        yield line


class B4Cleaner(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def setUp(self):
        self.result_candidate = self.result_candidate[0]

    def test_type(self):
        self.assertIsInstance(
            self.result_candidate,
            type(_gen_func),
            'Generator *function* must be passed as result (got %s)' % type(self.result_candidate)
        )
        self.assertIsInstance(
            self.result_candidate('abcd'),
            type(_gen_func()),
            'Call must create generator (got %s)' % type(self.result_candidate)
        )

    def test_transformation(self):
        input_dummy = "line\n\nline\n".splitlines()
        self.assertEqual(len(list(self.result_candidate(input_dummy))), 2, 'Remove empty lines (evaluate to False)')
        self.assertEqual(list(self.result_candidate(input_dummy)), list(clean_lines(input_dummy)))


class B4WhitespaceFixer(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def setUp(self):
        self.result_candidate = self.result_candidate[1]

    def test_type(self):
        self.assertIsInstance(
            self.result_candidate,
            type(_gen_func),
            'Generator *function* must be passed as result (got %s)' % type(self.result_candidate)
        )
        self.assertIsInstance(
            self.result_candidate('abcd'),
            type(_gen_func()),
            'Call must create generator (got %s)' % type(self.result_candidate)
        )
        self.assertIsInstance(
            self.result_candidate('abcd', 'sep'),
            type(_gen_func()),
            'Call must support separator for joining lines'
        )

    def test_transformation(self):
        input_dummy = "line\n line_cont\nline2\n line_cont2".splitlines()
        self.assertGreaterEqual(
            len(list(self.result_candidate(iter(input_dummy)))), len(list(fix_whitespace(iter(input_dummy)))),
            'Keep previous lines in a buffer until they cannot be joined',
        )
        self.assertLessEqual(
            len(list(self.result_candidate(iter(input_dummy)))), len(list(fix_whitespace(iter(input_dummy)))),
            'Join lines starting with a space to the previous line(s)',
        )
        self.assertEqual(list(self.result_candidate(iter(input_dummy))), list(fix_whitespace(iter(input_dummy))))


class B4WFieldSplitter(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def setUp(self):
        self.result_candidate = self.result_candidate[2]

    def test_type(self):
        self.assertIsInstance(
            self.result_candidate,
            type(_gen_func),
            'Generator *function* must be passed as result (got %s)' % type(self.result_candidate)
        )
        self.assertIsInstance(
            self.result_candidate('abcd'),
            type(_gen_func()),
            'Call must create generator (got %s)' % type(self.result_candidate)
        )
        self.assertIsInstance(
            self.result_candidate('abcd', 'sep'),
            type(_gen_func()),
            'Call must support separator for splitting lines'
        )

    def test_transformation(self):
        input_dummy = "line:line_cont:l1\nline2:line_cont2:l3".splitlines()
        self.assertGreaterEqual(
            len(list(self.result_candidate(input_dummy, ':'))), len(list(split_fields(input_dummy, ':'))),
            'Split lines at separator',
        )
        self.assertLessEqual(
            len(list(self.result_candidate(input_dummy, ':'))), len(list(split_fields(input_dummy, ':'))),
            'Split lines only at separator',
        )
        self.assertEqual(list(self.result_candidate(input_dummy, ':')), list(split_fields(input_dummy, ':')))


class B4WFieldParser(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def setUp(self):
        self.result_candidate = self.result_candidate[3]

    def test_type(self):
        self.assertIsInstance(
            self.result_candidate,
            type(_gen_func),
            'Generator *function* must be passed as result (got %s)' % type(self.result_candidate)
        )
        self.assertIsInstance(
            self.result_candidate('abcd'),
            type(_gen_func()),
            'Call must create generator (got %s)' % type(self.result_candidate)
        )

    def test_transformation(self):
        input_dummy = [('1', '"two"', '1.0', 'False')]
        self.assertEqual(list(self.result_candidate(input_dummy)), list(parse_fields(input_dummy)))
        for true_val in itertools.chain(trueish, falseish):
            input_dummy = [('1', '"two"', '1.0', repr(true_val))]
            self.assertEqual(
                list(self.result_candidate(input_dummy)), list(parse_fields(input_dummy)),
                'Interpret boolean aliases'
            )


def _b4_on_result(result, valid, passed_tests):
    applied_operations = 'open(titanic_path)'
    with open(titanic_path) as titanic_data:
        data_iter = titanic_data
        if len(passed_tests) >= 1:
            data_iter = result[0](data_iter)
            applied_operations = result[0].__name__ + '(' + applied_operations + ')'
        if len(passed_tests) >= 2:
            data_iter = result[1](data_iter, ',')
            applied_operations = result[1].__name__ + '(' + applied_operations + ", ',')"
        if len(passed_tests) >= 3:
            data_iter = result[2](data_iter, ':')
            applied_operations = result[2].__name__ + '(' + applied_operations + ", ':')"
        if len(passed_tests) >= 4:
            data_iter = result[3](data_iter)
            applied_operations = result[3].__name__ + '(' + applied_operations + ')'
        print('Reading stream from', applied_operations)
        for line in itertools.islice(data_iter, 1, 10):
            print(line)


b4 = exercise.Exercise(
    'Nested Generator Functions',
    sample_solution=(clean_lines, fix_whitespace, split_fields, parse_fields),
    data={'titanic_data': titanic_path},
    test_cases=(B4Cleaner, B4WhitespaceFixer, B4WFieldSplitter, B4WFieldParser),
    on_result=_b4_on_result,
    description='Create four generators: clean lines, fix whitespace, split lines, parse fields',
    hints=(
        'You must submit a tuple of generators: `result = (clean_lines, fix_whitespace, split_lines, parse_fields)`.',
        'Unittests will inspect each piece separately. Start small!',
        'Inspect the output to see what is going wrong.',
        'Look up `ast.literal_eval`.',
    ),
    hint_offset=2,
)
