import random
import math
import unittest

from gks2016rpylib import exercise

_b1_sequence = [random.gauss(50, 5) for _ in range(200)]
_b1_mean = sum(_b1_sequence) / len(_b1_sequence)
_b1_variance = sum((_b1_mean - element) ** 2 for element in _b1_sequence) / len(_b1_sequence)


class B1TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, float)

    def test_value(self):
        self.assertNotAlmostEqual(self.result_candidate, _b1_variance, 1, 'Not the variance')
        self.assertAlmostEqual(self.result_candidate, b1.sample_solution, 1)


b1 = exercise.Exercise(
    'Generator Expression',
    sample_solution=math.sqrt(sum((_b1_mean - element) ** 2 for element in _b1_sequence) / len(_b1_sequence)),
    data={
        'sequence': _b1_sequence,
    },
    test_cases=(B1TestCases,),
    description='Calculate the standard deviation of `sequence`',
    hints=(
        'Generator expression syntax is `(element for element in iterable)`.',
        'The sequence was generated with an SD of 5.',
        'The length and sum of a sequence can be computed with `len` and `sum`.',
        'You need the square of each element\'s distance to the mean, the number of elements, and a square root.',
    ),
    hint_offset=2,
)
