import unittest
import itertools

from gks2016rpylib import exercise


def vocals_generator():
    """Do not look at this implementation unless you got the easy case working :P"""
    yield from 'aeiou'


class BTestGeneratorFunctionType(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(
            self.result_candidate,
            type(vocals_generator),
            'Generator *function* must be passed as result (got %s)' % type(self.result_candidate)
        )
        self.assertIsInstance(
            self.result_candidate(),
            type(vocals_generator()),
            'Call must create generator (got %s)' % type(self.result_candidate)
        )


class B2TestContent(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_value(self):
        self.assertEqual(
            len(list(itertools.islice(self.result_candidate(), len('aeiou')))),
            len('aeiou'),
            'Produces enough elements',
        )
        self.assertLess(
            len(list(itertools.islice(self.result_candidate(), len('aeiou')))),
            len('aeiou') + 1,
            'Produces correct number of elements',
        )
        self.assertEqual(
            ''.join(itertools.islice(self.result_candidate(), len('aeiou'))),
            'aeiou',
        )


b2 = exercise.Exercise(
    'Generator Function',
    sample_solution=vocals_generator(),
    data={},
    test_cases=(BTestGeneratorFunctionType, B2TestContent,),
    description='Create a generator function for the vocals `aeiou`',
    hints=(
        'Generator functions are defined by `yield value` in their body.',
        'Multiple `yield` statements are allowed in a generator function.',
        'The function must `yield` each letter separately.',
    ),
    hint_offset=2,
)
