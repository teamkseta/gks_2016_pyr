import random
import unittest

from gks2016rpylib import exercise

_a1_iterable = [str(random.random() * 100) for _ in range(20)]


class A1TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_container_type(self):
        self.assertIsInstance(self.result_candidate, list)

    def test_element_type(self):
        types_in_result = {type(elem) for elem in self.result_candidate}
        self.assertIn(float, types_in_result)
        self.assertEqual(len(types_in_result), 1, 'Same type for all elements')

    def test_value(self):
        self.assertEqual(self.result_candidate, a1.sample_solution)


a1 = exercise.Exercise(
    'List Comprehension',
    sample_solution=[float(a) for a in _a1_iterable],
    data={
        'iterable': _a1_iterable
    },
    test_cases=(A1TestCases,),
    description='Use list comprehension to generate the squares from `namespace.iterable`.',
    hints=(
        'List comprehension syntax is `[element for element in iterable]`',
        'Convert an element via `float(element)`.'
    ),
    hint_offset=1,
)
