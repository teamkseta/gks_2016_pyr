import socket
import random
import unittest

from gks2016rpylib import exercise

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))
    localhost_ip = s.getsockname()[0]
except Exception:
    localhost_ip = '.'.join(str(random.randint(1, 255)) for _ in range(4))

_a_4_config_str = r"""
user.users = bwayne, ckent, dgrayson, apenny
user.root = bman

bsec.random = OFF, NO_JUNK, 64, ZLIB, UCODE
bsec.pub = b'KNUW24DMMUWCA2LTNYTXIIDJOQ7Q===='
bsec.auth = z'x\x9c\xf3\xcb/W(\xc9H,Q/VHTH\xcf\xcfOQH/M-.V\x04\x00h4\x08R'
bsec.key = u16'鱸佳⤭쳉坋줨ⵈ핊폓\x03⬳锅', b'x\x9c+.)\xb2*\xb0JR\x8f\xa9\xb00\x88\xa900v\x8e\xa90L\x0e3\xca(\x8b\x0cvM\x0f\x0f\xb74\xf4tw\xab\x8c\nv\x8aH\x0c7-\x8d\x8a\xf0\xcc(\x04\xaa2\xd0S\x07\x00\x0e1\x12\x10'

monitor.target = *.gpd.gov, knifes-and-lint.com, *.hdent.org
monitor.inform = mail:%s@%s
monitor.level = ALL, NOPRIV
""".strip() % ('%8X' % random.getrandbits(4*8), localhost_ip)


class A4TestContainer(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_container_type(self):
        self.assertIsInstance(self.result_candidate, dict)


class A4TestContent(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_key_type(self):
        types_in_result = {type(elem) for elem in self.result_candidate.keys()}
        self.assertIn(str, types_in_result)
        self.assertEqual(len(types_in_result), 1, 'Same type for all elements')

    def test_value_type(self):
        types_in_result = {type(elem) for elem in self.result_candidate.values()}
        self.assertIn(list, types_in_result)
        self.assertEqual(len(types_in_result), 1, 'Same type for all elements')


class A4TestResult(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_key_format(self):
        self.assertEqual(
            list(self.result_candidate.keys()),
            [key.strip() for key in self.result_candidate.keys()],
            'White space `strip`d from key'
        )

    def test_value_format(self):
        self.assertEqual(
            list(self.result_candidate.values()),
            [[value.strip() for value in values] for values in self.result_candidate.values()],
            'White space `strip`d from value'
        )

    def test_value(self):
        self.assertEqual(self.result_candidate, a4.sample_solution)


a4 = exercise.Exercise(
    'Nested Dict and List Comprehension',
    sample_solution={
        key.strip(): [val.strip() for val in value.split(', ')]
        for key, value in [line.split('=', 1) for line in _a_4_config_str.splitlines() if line]
    },
    data={
        'config_str': _a_4_config_str
    },
    test_cases=(A4TestContainer, A4TestContent, A4TestResult),
    description='Read the ini-style string `config_str` to a dictionary.',
    hints=(
        'Dict comprehension syntax is `{key: value for element in iterable}`.',
        'List comprehension syntax is `[element for element in iterable]`.',
        'You will need string methods to split and strip. Use tab completion if you don\'t know them.',
        'A string can be split to lines via `my_str.splitlines()`.',
        'A `key = value` line can be split via `line.split(=)`.',
        'There is a second, optional parameter for `line.split`.',
        'Ignore lines that cannot be split!',
        'An empty is considered `False` in an `if` statement.',
        'Divide and succeed: a list of keyvalues, a key and a list of values.',
        'You need something like this: {k: [v1, v2, ...] for k, v in [k, v for kv in input if kv]}'
    ),
    hint_offset=3,
)