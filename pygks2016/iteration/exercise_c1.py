import collections
import datetime
import time
import random
import math
import threading
import unittest
import timeit

from gks2016rpylib import exercise


class Probe:
    """
    Temperature probe for Karlsruhe

    Horrendously optimistic when it's not summer.
    """
    @staticmethod
    def _query():
        now = datetime.datetime.now()
        now = datetime.datetime(
            year=now.year, month=now.month, day=now.day, hour=now.hour, minute=now.minute, second=now.second,
            microsecond=now.microsecond // 1000 * 1000
        )
        now_seconds = 60 * 60 * now.hour + 60 * now.minute + now.second
        # let's assume a nice day in Karlsruhe
        now_temp = 20 - 10 * math.cos(((now_seconds / (60*60*12)) - 0.3) * math.pi) + random.gauss(0, 0.01)
        return str(now), '%.2f' % now_temp, '°C'

    def subscribe(self, container, ticks=100):
        """
        Fill `container` with data for the next `ticks` sampling intervals

        :param container: container to store data in; must implement `append(value)`
        :param ticks: number of samples to store
        """
        if not hasattr(container, 'append'):
            raise TypeError('`container` must support append of value(s)')

        def push():
            for _ in range(ticks):
                container.append(self._query())
                time.sleep(0.00075)
        collector = threading.Thread(target=push)
        collector.start()
        return collector


class RingBuffer(object):
    """
    FIFO Ring Buffer with destructive iteration

    :param length: maximum size of the buffer before discarding items
    :type length: int
    """
    def __init__(self, maxlen=None):
        self._buffer = collections.deque(maxlen=maxlen)

    def append(self, value):
        """Push a value into the buffer. Discards the oldest value if full."""
        self._buffer.append(value)

    def __iter__(self):
        while self._buffer:
            yield self.pop()

    def pop(self):
        """Remove and return the oldest value from the buffer."""
        return self._buffer.popleft()

    def extend(self, iterable):
        """Push all values from iterable into the buffer. Discards the oldest value(s) if full."""
        self._buffer.extend(iterable)

    def __len__(self):
        return len(self._buffer)

    def __str__(self):
        # nice formatting for your class when printed
        return '<%s]' % ', '.join(repr(item) for item in self._buffer)

    def __repr__(self):
        # technical formatting for your class for debug output
        return '%s(%s)' % (self.__class__.__name__, self)


class ThinRingBuffer(RingBuffer):
    """
    Thinner interface around the builtin
    """
    def __init__(self, maxlen=None):
        super().__init__(maxlen=maxlen)
        self.append = self._buffer.append
        self.pop = self._buffer.popleft
        self.extend = self._buffer.extend


class TypeTester(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, type(RingBuffer), 'A class is required')

    def test_methods(self):
        self.assertTrue(hasattr(self.result_candidate, '__init__'), 'Initializer method (__init__)')
        self.assertTrue(hasattr(self.result_candidate, 'append'), 'Append data method (append)')
        self.assertTrue(hasattr(self.result_candidate, '__iter__'), 'Iteration method (__iter__)')


class InterfaceTester(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_init(self):
        with self.assertRaises(TypeError):
            self.result_candidate(maxlen='NotANumber')

    def test_append(self):
        buffer = self.result_candidate()
        self.assertIsNone(buffer.append('String'), 'Append data')
        self.assertIsNone(buffer.append(('Tuple', 'Sequence')), 'Append container data')

    def test_iter(self):
        buffer = self.result_candidate()
        self.assertIsInstance(iter(buffer), type(iter(RingBuffer())), '`__iter__` returns iterable.')


class BufferTester(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_buffering(self):
        for data_len in (1, 5, 10, 100):
            for buffer_len in (20, 10, 1):
                result_buffer = self.result_candidate(maxlen=buffer_len)
                sample_buffer = RingBuffer(maxlen=buffer_len)
                for val in range(data_len):
                    result_buffer.append(val)
                    sample_buffer.append(val)
                self.assertEqual(len(list(sample_buffer)), min(data_len, buffer_len), 'Consistency! Scream if this breaks')
                self.assertEqual(len(list(result_buffer)), min(data_len, buffer_len), 'Buffer must respect maxlen')
                self.assertEqual(list(result_buffer), list(sample_buffer))

# Py <= 3.5 does not support globals for timeit
# need to transfer it dynamically via this module

_time_cls = [None]

timeit_append_setup = """
from %(module)s import _time_cls
from collections import deque
test_buffer = _time_cls[0](maxlen=%%(maxlen)s)
test_consumer = deque(maxlen=1)
""" % {'module': __name__}

timeit_append_stmt = """
test_buffer.append(255)
"""

timeit_append_pop_stmt = """
test_buffer.append(255)
test_buffer.pop()
"""

timeit_extend_range_stmt = """
test_buffer.extend(range(%(item_count)s))
"""

timeit_iter_range_stmt = """
test_buffer.extend(range(%(item_count)s))
test_consumer.extend(test_buffer)
"""


def benchmark(buffer_class, valid, passed):
    """
    Benchmark a submitted RingBuffer compared to sample solutions

    Note: Please do not look at the implementation, it's ugly. ;)
    """
    if not valid:
        return

    def print_results(result):
        print('Yours:', '%.3f' % result['y'], 'Sample:', '%.3f' % result['s'], 'Thin:', '%.3f' % result['t'])

    time.sleep(0.25)  # give Jupyter time to reap stdout/stderr
    targets = {'y': buffer_class, 's': RingBuffer, 't': ThinRingBuffer}
    append_times, append_tries, append_len = 100000, 10, 100
    append_results = {}
    print('== Timing: append             x %d, 1 to len=%d (Best of %d) ==' % (append_times, append_len, append_tries))
    for name in targets:
        _time_cls[0] = targets[name]
        timer = timeit.repeat(
            stmt=timeit_append_stmt,
            setup=timeit_append_setup % {'maxlen': append_len},
            repeat=append_tries,
            number=append_times,
        )
        append_results[name] = min(timer)
    print_results(append_results)
    append_times, append_tries, append_len = 100000, 10, 100
    append_results = {}
    print('== Timing: append + pop       x %d, 1 to len=%d (Best of %d) ==' % (append_times, append_len, append_tries))
    for name in targets:
        _time_cls[0] = targets[name]
        timer = timeit.repeat(
            stmt=timeit_append_pop_stmt,
            setup=timeit_append_setup % {'maxlen': append_len},
            repeat=append_tries,
            number=append_times,
        )
        append_results[name] = min(timer)
    print_results(append_results)
    append_times, append_tries, append_len, append_size = 100000, 10, 10, 100
    append_results = {}
    print('== Timing: extend             x %d, %d to len=%d (Best of %d) ==' % (append_times, append_size, append_len, append_tries))
    for name in targets:
        _time_cls[0] = targets[name]
        if hasattr(targets[name], 'extend'):
            timer = timeit.repeat(
                stmt=timeit_extend_range_stmt % {'item_count': append_size},
                setup=timeit_append_setup % {'maxlen': append_len},
                repeat=append_tries,
                number=append_times,
            )
            append_results[name] = min(timer)
        else:
            append_results[name] = '----'
    print_results(append_results)
    append_times, append_tries, append_len, append_size = 100000, 10, 10, 100
    append_results = {}
    print('== Timing: extend + iter      x %d, %d to len=%d (Best of %d) ==' % (append_times, append_size, append_len, append_tries))
    for name in targets:
        _time_cls[0] = targets[name]
        if hasattr(targets[name], 'extend'):
            timer = timeit.repeat(
                stmt=timeit_iter_range_stmt % {'item_count': append_size},
                setup=timeit_append_setup % {'maxlen': append_len},
                repeat=append_tries,
                number=append_times,
            )
            append_results[name] = min(timer)
        else:
            append_results[name] = '----'
    print_results(append_results)


c1 = exercise.Exercise(
    'Iterable Ring Buffer',
    sample_solution=RingBuffer,
    data={'probe': Probe()},
    test_cases=(TypeTester, InterfaceTester, BufferTester),
    on_result=benchmark,
    description='Create a (ring-) buffer of limited size and destructive iteration',
    hints=(
        'Write a class implementing an internal container and public interface.',
        'You must provide iteration and the following methods: append, pop, extend.',
        '`__iter__` should extract the oldest value from the container and `yield` it.',
        'Look up the `collections.deque` class to contain your data.',
        'You only need `maxlen`, `append` and `popleft` of the deque.',
    ),
    hint_offset=2,
)
