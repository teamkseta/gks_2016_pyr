import unittest

from gks2016rpylib import exercise

_a2_characters = ''.join(chr(el) for char_range in (range(ord('A'), ord('Z') + 1), range(ord('a'), ord('z') + 1)) for el in char_range)


def rot13(char):
    """
    Calculate the ROT13 substitute for `char`

    :param char: The character to substitute
    :type char: str
    :returns: The substitute
    :rtype: str
    """
    if ord('a') <= ord(char) <= ord('z'):
        return chr((ord(char) - ord('a') + 13) % 26 + ord('a'))
    elif ord('A') <= ord(char) <= ord('Z'):
        return chr((ord(char) - ord('A') + 13) % 26 + ord('A'))
    raise ValueError('Not an ASCII letter: %r' % char)


class A2TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_container_type(self):
        self.assertIsInstance(self.result_candidate, dict)

    def test_element_type(self):
        types_in_result = {type(elem) for key_values in self.result_candidate for elem in key_values}
        self.assertIn(str, types_in_result)
        self.assertEqual(len(types_in_result), 1, 'Same type for all elements')

    def test_value(self):
        self.assertEqual(
            [self.result_candidate[char] for char in _a2_characters],
            [a2.sample_solution[char] for char in _a2_characters],
            'Same result of encoding'
        )


def _a2_on_result(result, valid, passed_tests):
    result = result if isinstance(result, dict) else {}
    real_str = 'The Quick Brown Fox Jumps Over the Lazy Dog. Now Pack My Box With Five Dozen Liquor Jugs.'
    if valid:
        real_str += ' And you made it, by the way!'
    encoded_str = ''.join(
        a2.sample_solution.get(char, char)
        for char in
        real_str
    )
    print(''.join(result.get(char, char) for char in encoded_str))


a2 = exercise.Exercise(
    'Dict Comprehension',
    sample_solution={char: rot13(char) for char in _a2_characters},
    data={
        'letters': _a2_characters,
        'rot13': rot13,
    },
    test_cases=(A2TestCases,),
    description='Create a mapping to ROT13 for `letters`.',
    hints=(
        'Dict comprehension syntax is `{key: value for element in iterable}`',
        'Each `key` is the element of iterable. You must use a function to compute `value` from `key`.'
        'The expression should look like `element: cipher_func(element)`.',
        'If you write your own `rot13` function, use the `ord` and `chr` builtins.',
    ),
    hint_offset=1,
    on_result=_a2_on_result,
)