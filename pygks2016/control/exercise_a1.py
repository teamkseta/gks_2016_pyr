import random
import itertools
import unittest

from gks2016rpylib import exercise

b_mapper = {chr(num): num for num in range(ord('a'), ord('z'))}
a_mapper = {num: '%01.3f' % num for num in (random.gauss(50, 20) for _ in range(20))}

# shuffle
for _ in range(3):
    for key in list(b_mapper.keys()):
        if random.random() < 0.3:
            a_mapper[key] = b_mapper[key]
            del b_mapper[key]
    for key in list(a_mapper.keys()):
        if random.random() < 0.3:
            b_mapper[key] = a_mapper[key]
            del a_mapper[key]

keys = [key for key in itertools.chain(b_mapper, a_mapper) if random.random() > 0.5]

all_mapper = {}
for key in keys:
    try:
        all_mapper[key] = b_mapper[key]
    except KeyError:
        all_mapper[key] = a_mapper[key]


class ValueTester(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, dict)

    def test_content(self):
        self.assertEqual(self.result_candidate, all_mapper)


a1 = exercise.Exercise(
    'Exceptions for Control Flow',
    sample_solution=all_mapper,
    data={'keys': keys, 'a_mapper': a_mapper, 'b_mapper': b_mapper},
    test_cases=(ValueTester,),
    on_result=None,
    description='Fetch the values for each key from the appropriate mapper',
    hints=(
        'You need to try: one collection, and the other on except:.',
    ),
    hint_offset=1,
)
