import random
import itertools
import unittest

from gks2016rpylib import exercise


def raiser(val):
    if not isinstance(val, int):
        raise ValueError


class ValueTester(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_raise(self):
        with self.assertRaises(ValueError):
            self.result_candidate('NAN')

    def test_raises_not(self):
        self.assertIsNone(self.result_candidate(3))


a2 = exercise.Exercise(
    'Raising Exceptions',
    sample_solution=raiser,
    data={},
    test_cases=(ValueTester,),
    on_result=None,
    description='Write a function that raises ValueError on non-integer input.',
    hints=(
        'Use `isinstance` to check type.',
        'Use the `raise` statement, e.g. `raise ValueError`.'
    ),
    hint_offset=1,
)
