import datetime
import time
import random
import math
import unittest

from gks2016rpylib import exercise
import pygks2016.iteration.exercise_c1


class Probe(pygks2016.iteration.exercise_c1.Probe):
    ENODAT = -1
    ENOBOCK = -2

    def __init__(self):
        self._last_poll = 0

    def poll(self):
        """C-Binding, gets query or returns ENODAT or ENOBOCK"""
        if random.random() < 0.2:
            return self.ENOBOCK
        elif time.time() - self._last_poll < 0.5:
            return self.ENODAT
        else:
            self._last_poll = time.time()
            return self._query()


class NoData(Exception):
    """The Probe has no data at the moment"""


def query_probe(probe):
    while True:
        retval = probe.poll()
        if retval == -1:
            raise NoData
        elif retval != -2:
            return retval


class InterfaceTester(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_init(self):
        probe = Probe()
        self.assertIsNotNone(self.result_candidate(probe))
        with self.assertRaises(Exception):
            self.result_candidate(probe)


class ExceptionTester(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_init(self):
        probe = Probe()
        self.assertIsNotNone(self.result_candidate(probe))
        with self.assertRaises(Exception) as err:
            self.result_candidate(probe)
        self.assertIsNot(type(err), Exception)


a3 = exercise.Exercise(
    'Raising Exceptions',
    sample_solution=query_probe,
    data={'Probe': Probe},
    test_cases=(InterfaceTester, ExceptionTester),
    on_result=None,
    description='Write a function to query a `Probe` object.',
    hints=(
        'You must `raise` an exception if no data is available.',
        'Check which error codes are returned by the function.',
        'There is another state than "data" and "nodata".',
        'return Probe.ENOBOCK',
    ),
    hint_offset=1,
)
