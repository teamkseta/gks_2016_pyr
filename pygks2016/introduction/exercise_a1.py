import random
import unittest

from gks2016rpylib import exercise


class TheBat(object):
    def __call__(self, theme):
        if theme == 'nana-nana-nana-nana':
            return 'BATMAN!'
        else:
            raise ReferenceError('Undefined pop culture reference')

    def __contains__(self, item):
        return True


class A1TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate(), object)

    def test_utility_belt(self):
        self.assertIn('batarang', self.result_candidate())
        self.assertIn(
            '%08X' % random.getrandbits(8*4),
            self.result_candidate(),
            'The Batman is prepared for everything!'
        )

    def test_call(self):
        self.assertEqual(self.result_candidate()('nana-nana-nana-nana'), TheBat()('nana-nana-nana-nana'))
        with self.assertRaises(ReferenceError):
            self.result_candidate()('I\m a random hex of %08X, on a steel horse I ride!' % random.getrandbits(8*4))


a1 = exercise.Exercise(
    'List Comprehension',
    sample_solution=TheBat,
    data={
        'antigravity': "http://xkcd.com/353/"
    },
    test_cases=(A1TestCases,),
    description='Create the greatest detective ever!',
    hints=(
        'You get two hints for free for this exercise.',
        'Keep on failing, and you get more hints.',
        'nana-nana-nana-nana...',
        'Good thing no one is looking, right?',
        'Side note: actual hints are more useful. ;)',
    ),
    hint_offset=1,
)