import os
import unittest

from gks2016rpylib import exercise

_data_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")
_a1_raw_files = [os.path.join(_data_path, file_name) for file_name in os.listdir(_data_path)]


class A1TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, int, "Please specify the integer value")

    def test_value(self):
        self.assertEqual(57560, self.result_candidate)


class A2TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, list, "Please specify a list of names")

    def test_value(self):
        self.assertEqual(["David", "James", "Andrew", "John", "Christopher", "Scott", "Emma",
                          "Michael", "Craig", "Laura"], self.result_candidate)


class A3TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, dict, "Please specify a dict of names for boys and girls")

    def test_keys(self):
        self.assertIsNotNone(self.result_candidate.get("boys", None))
        self.assertIsNotNone(self.result_candidate.get("girls", None))

    def test_value(self):
        self.assertEqual(["David", "James", "Andrew", "John", "Christopher", "Scott", "Michael",
                          "Craig", "Paul", "Ryan"], self.result_candidate.get("boys", None))
        self.assertEqual(["Emma", "Laura", "Sarah", "Claire", "Nicola", "Jennifer", "Amy", "Lauren",
                          "Louise", "Rebecca"], self.result_candidate.get("girls", None))

exercise_a1 = exercise.Exercise(
    'Number of unique Names',
    sample_solution=0,
    data={
        'raw_files': _a1_raw_files
    },
    test_cases=(A1TestCases,),
    description='Given a list of used names from scotland we want to determine the number of unique names.',
    hints=(
        'You can use the function `unique` to determine unique names.',
        'You can use the function `length` to determine the number of unique names.',
        'Please specify the integer value, not the `IntVector`.'
    )
)

exercise_a2 = exercise.Exercise(
    'Top 10 Names',
    sample_solution=0,
    data={},
    test_cases=(A2TestCases,),
    description='Given a list of names from scotland we want to know their top 10.',
    hints=(
        'You can use the function `head` to get the first elements.',
        'You can specify an R function that uses function order to specify a sorting.'
    )
)

exercise_a3 = exercise.Exercise(
    'Top 10 Boys and Girls',
    sample_solution=0,
    data={},
    test_cases=(A3TestCases,),
    description='Given a list of names from scotland we want to know their top 10 for boys and girls.',
    hints=(
        'You can use the function `head` to get the first elements.',
        'You can specify an R function that uses function order to specify a sorting.',
        'You can use the method `filter` from `dplyr` to only get values for one sex.'
    )
)
