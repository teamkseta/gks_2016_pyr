import random
import unittest

from gks2016rpylib import exercise


random.seed(1337)
_data = [str(random.random()) for _ in range(100)]


class A1TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, tuple)

    def test_values(self):
        fourth, tenth, fifties = self.result_candidate
        self.assertAlmostEqual(0.06673041, fourth, 2, "Your given value for 4th percentile should match within 2 places")
        self.assertAlmostEqual(0.164675, tenth, 2, "Your given value for 10th percentile should match within 2 places")
        self.assertAlmostEqual(0.5731367, fifties, 2, "Your given value for 50th percentile should match within 2 places")

exercise_a1 = exercise.Exercise(
    'Getting percentiles from data',
    sample_solution=0,
    data={
        'data_string': _data
    },
    test_cases=(A1TestCases,),
    description='Use a simple argument list to determine a given percentile from a self-written R script.',
    hints=(
        'The function `quantile` can be used to get a given percentile.',
        'Remember to remove the last element if this defines your percentile'
    )
)
