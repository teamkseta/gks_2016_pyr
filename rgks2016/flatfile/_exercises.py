import os
import unittest

from gks2016rpylib import exercise

_a1_raw_file = os.path.join(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), "data"), "titanic_passengers.csv"
)


class A1TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, dict)

    def test_keys(self):
        self.assertIsNotNone(self.result_candidate.get("male", None))
        self.assertIsNotNone(self.result_candidate.get("female", None))

    def test_values(self):
        self.assertAlmostEqual(0.1670588, self.result_candidate.get("male", None), 2)
        self.assertAlmostEqual(0.6630670, self.result_candidate.get("female", None), 2)

exercise_a1 = exercise.Exercise(
    'Surviving passengers',
    sample_solution=0,
    data={
        'raw_file': _a1_raw_file
    },
    test_cases=(A1TestCases,),
    description='From passenger data of Titanic the survivors grouped by sex are determined.',
    hints=(
        'The ratio should be determined grouped by sex (male/female).',
        'Did you realise that there are different possibilities for true in survived?'
    )
)
