import os

from gks2016rpylib import exercise

_data_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "logs")
_a1_raw_files = [os.path.join(_data_path, file_name) for file_name in os.listdir(_data_path)]


def _convert_to_seconds(value):
    seconds = 0
    # check for hours
    values = value.split("h")
    if len(values) > 1:
        seconds += float(values[0])*60*60
        values.pop(0)
    # check for minutes
    values = values[0].split("m")
    if len(values) > 1:
        seconds += float(values[0])*60
        values.pop(0)
    # and add seconds
    values = values[0].split("s")
    if len(values) > 1:
        seconds += float(values[0])
        values.pop(0)
    return seconds

exercise_a1 = exercise.Exercise(
    'Analysing uncleaned data',
    sample_solution=0,
    data={
        'raw_files': _a1_raw_files,
        'convert_to_seconds': _convert_to_seconds,
        'runs': 5
    },
    test_cases=(),
    description='Use pyRserve to analyse data that needs to be cleaned first.',
    hints=(
        'Consider that you need to look at two sections (serial/parallel)',
        'Consider, that there is a complete section that needs to be ignored.'
    )
)
