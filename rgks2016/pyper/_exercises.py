import os
import unittest

from gks2016rpylib import exercise

_a1_raw_file = [os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)), "data"),
                            "289478468_62014_5927_airline_delay_causes.csv")]


class A1TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, float)

    def test_range(self):
        self.assertTrue(self.result_candidate > 1)

    def test_value(self):
        self.assertAlmostEqual(79.84424661172406, self.result_candidate, 1)


class A2TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, str)

    def test_value(self):
        self.assertEqual("HA", self.result_candidate)


class A3TestCases(unittest.TestCase):
    #: Object to test
    result_candidate = None

    def test_type(self):
        self.assertIsInstance(self.result_candidate, str)

    def test_value(self):
        self.assertEqual("EV", self.result_candidate)

exercise_a1 = exercise.Exercise(
    'Analysing flight data',
    sample_solution=0,
    data={
        'raw_file': _a1_raw_file
    },
    test_cases=(A1TestCases,),
    description='Based on available flight statistics you are supposed to determine ontime flights..',
    hints=(
        'Remember to remove NA values from your data.',
        'Take care, the question asks for ontime flights, not delayed flights.',
        'We are expecting a percentage here, no proportions.'
    )
)

exercise_a2 = exercise.Exercise(
    'Getting the best carrier',
    sample_solution=0,
    data={
    },
    test_cases=(A2TestCases,),
    description='Based on available flight statistics determine the carrier with best ontime rates.',
    hints=(
        'Remember to remove NA values from your data.',
        'Take care, the question asks for carrier, not carrier_name.',
        'Look at `dplyr` to better handle your data.',
        'The first item of a dataset can be determined with `head`.'
    )
)

exercise_a3 = exercise.Exercise(
    'Getting the worst carrier',
    sample_solution=0,
    data={
    },
    test_cases=(A3TestCases,),
    description='Based on available flight statistics determine the carrier with worst ontime rates.',
    hints=(
        'Remember to remove NA values from your data.',
        'Take care, the question asks for carrier, not carrier_name.',
        'Look at `dplyr` to better handle your data.',
        'The last element of a dataset can be determined with `tail`.'
    )
)
