import sys
import operator

def si_power(number, base=1000):
    """
    Calculate the SI prefix, power and 3-character format of number
    """
    max_power = 6
    for t_power, prefix in enumerate(u'EPTGMk mμnpfa'):
        power = max_power - t_power
        p_num = number / (base ** power)
        if 1000 > p_num >= 1.0:
            if p_num > 100:
                return '%3.0f' % p_num, prefix, power
            if p_num > 10:
                return '%2.0f' % p_num, prefix, power
            return '%3.1f' % p_num, prefix, power
    else:
        raise ValueError


class bytesize(int):
    """Integer pretty-printed as size in Byte"""
    def __str__(self):
        return '%3s%sB' % si_power(self, 1024)[:2]

    def __repr__(self):
        return '%s(%d~=%s)' % (self.__class__.__name__, self, self)

    def _operator(op, reverse=False):
        if not reverse:
            def opfunc(self, other):
                if isinstance(self, type(other)):
                    return bytesize(op(int(self), other))
                return NotImplemented
            opfunc.__name__ = op.__name__
        else:
            def opfunc(self, other):
                if isinstance(self, type(other)):
                    return bytesize(op(int(self), other))
                return NotImplemented
            opfunc.__name__ = '__r' + op.__name__.rstrip('__')
        return opfunc

    __add__, __radd__ = _operator(operator.__add__), _operator(operator.__add__, reverse=True)
    __mul__, __rmul__ = _operator(operator.__mul__), _operator(operator.__mul__, reverse=True)
    __sub__, __rsub__ = _operator(operator.__sub__), _operator(operator.__sub__, reverse=True)
    del _operator


def getbytesizeof(obj):
    """Get the size of an object as :py:class:`~.bytesize`"""
    return bytesize(sys.getsizeof(obj))
