import threading
import unittest
import io
import sys
import textwrap

_help_formatter = textwrap.TextWrapper(
    width=120,
    expand_tabs=True,
    tabsize=4,
    initial_indent='  ',
    replace_whitespace=False
)


class NameSpace(object):
    """
    Write-only attribute namespace

    You can get a list of all defined names with `list(namespace)`.
    """
    def __init__(self, **kw_values):
        self._key_values = kw_values

    @property
    def __all__(self):
        return list(self._key_values.keys())

    def __getitem__(self, item):
        try:
            return self._key_values[item]
        except KeyError:
            raise NameError('name %r is not defined' % item)

    def __getattr__(self, item):
        return self[item]

    def __iter__(self):
        return iter(self._key_values)

    def __str__(self):
        return '%s%s' % (self.__class__.__name__, sorted(self))

    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__, self._key_values)


default_hint = """
Sorry, no hints for this exercise!
Just ask us if you have a problem.
""".strip()


class Exercise(object):
    """
    An exercise for you to do! Nice!

    Instances of this class hold everything needed to perform a single exercise:

    * The input/data is available in `exercise.data`. You can get a list
      of variables from `list(exercise.data)`. As a shorthand, you may also
      get input/data directly from the exercise object.

    * Submit your results by setting `exercise.result = your_result`. This will
      run a test-suite and tell you if something is missing.

    * At any time, you can use the `show_*` methods to get help.

       + `exercise.show_about()` displays the assignment and namespace.

       + `exercise.show_hint()` displays hints; for some assignments, more hints
         are revealed if your submitted results didn't work.

       + `exercise.show_help()` combines both displays.
    """
    def __init__(
            self,
            display_name=None, sample_solution=None, test_cases=(), data=None, on_result=None,
            description=None, hints=(default_hint,), hint_offset=0
    ):
        self._lock = threading.RLock()
        self.nick = display_name
        self.sample_solution = sample_solution
        self.data = NameSpace(**(data or {}))
        self._on_result = on_result
        self._description = description
        self._hints = hints
        self._hint_offset = hint_offset
        self._test_cases = test_cases
        self._last_result = NotImplemented
        self._valid_results = 0
        self._failed_results = 0

    def __getitem__(self, item):
        return self.data[item]

    def __getattr__(self, item):
        return self[item]

    def show_about(self):
        print(_help_formatter.fill(self._description))
        print()
        print(_help_formatter.fill("Namespace: '%s'" % "', '".join(self.data)))

    def show_help(self):
        print(_help_formatter.fill(self._description))
        print()
        self.show_hint()
        print()
        print(_help_formatter.fill("Namespace: '%s'" % "', '".join(self.data)))

    def show_hint(self):
        hint_count = self._failed_results + self._hint_offset
        if hint_count == 0:
            print(_help_formatter.fill(
                'Hint: Give it a try first.'
            ))
        elif hint_count < len(self._hints):
            print('Hint:')
            for hint in self._hints[:hint_count]:
                print(_help_formatter.fill(hint))
            print('...')
        else:
            print('Hint:')
            for hint in self._hints:
                print(_help_formatter.fill(hint))

    @property
    def result(self):
        with self._lock:
            if self._last_result is NotImplemented:
                return self.sample_solution
            return self._last_result

    @result.setter
    def result(self, value):
        with self._lock:
            if value is self.sample_solution:
                print('You little rebel. I like you. Now do it right.', file=sys.stderr)
            else:
                valid, passed = self._test_result(value)
                if valid:
                    self._valid_results += 1
                    self._last_result = value
                    self._post_result_validation(value, valid, passed)
                else:
                    self._failed_results += 1
                    self._post_result_validation(value, valid, passed)
                    self.show_hint()

    def _test_result(self, result):
        # try each test case consecutively to avoid lengthy output
        # fail fast at the earliest unsuccessful test case
        passed_tests = []
        for test_case in self._test_cases:
            # inject candidate into test suite
            class CurrentTest(test_case):
                __qualname__ = test_case.__qualname__
                result_candidate = staticmethod(result)

            stream = io.StringIO()
            test_runner = unittest.TextTestRunner(stream=stream)
            test_result = test_runner.run(unittest.defaultTestLoader.loadTestsFromTestCase(CurrentTest))
            if not test_result.wasSuccessful():
                if passed_tests:
                    print('Passed', len(passed_tests), '/', len(self._test_cases), 'Test Suites', file=sys.stderr)
                    print(*passed_tests, sep='\n', file=sys.stderr)
                print(stream.getvalue(), file=sys.stderr)
                return False, passed_tests
            else:
                passed_tests.append(test_case.__qualname__)
        print('Passed', len(passed_tests), '/', len(self._test_cases), 'Test Suites', file=sys.stderr)
        return True, passed_tests

    def _post_result_validation(self, result, valid=False, passed_tests=()):
        if self._on_result is not None:
            self._on_result(result, valid, passed_tests)
